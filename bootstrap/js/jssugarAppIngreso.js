var abrirIngreso = document.getElementById('abrirIngreso'),
	overlayIngreso = document.getElementById('overlayIngreso'),
	popupIngreso = document.getElementById('popupIngreso'),
	btnCerrarPopupIngreso = document.getElementById('btn-cerrar-popupIngreso');

    abrirIngreso.addEventListener('click', function(){
	overlayIngreso.classList.add('active');
	popupIngreso.classList.add('active');
});

btnCerrarPopupIngreso.addEventListener('click', function(e){
	e.preventDefault();
	overlayIngreso.classList.remove('active');
	popupIngreso.classList.remove('active');
});