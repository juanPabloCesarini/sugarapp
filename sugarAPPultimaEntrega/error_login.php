<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>sugarAPP | Login</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Vollkorn" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Staatliches" rel="stylesheet">
	<link rel="stylesheet" href="css/index.css">
</head>
<body>
	<div class="contenedor">
		<div class ="header">
			<h1>sugarAPP</h1>
			<h3>La aplicación que necesitabas</h3>
        </div>
		<div class="sidebar"></div>
		<div class="form">
			<form action="" class="formularioLogin" method="post">
				<img src="img/logoSugarAPP.jpg" alt="">
				<input type="text" name="usuario" id="usuario" class="usuario" placeholder="Email:">
				<input type="password" name="clave" id="clave "class="contras" placeholder="Password:">
				<div class="error" id="error">Email / Contraseña Incorrecto</div>
				<button class="boton">Ingresar</button>
				<a href="registro_form.php">Soy Nuevo</a>
				<a href="recupero.html">Perdí la contraseña</a>	
			</form>
		</div>
		<div class="sidebar"></div>
		<div class="footer">
			<h6> 2019 - Todos los derechos Reservados. <br> Realizado por: Cesarini - Figueras - Grillo - Gutierrez - Larossa</h6>
		</div>
	</div>
</body>
<?php	include("PHP/login.php");?>
</html>